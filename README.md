# Challenge

## Task 1 - Vakum Cleaner Robot

Let's implement a vacuum cleaner robot. The robot has battery and each of a robot command cost an energy. The main goal is clean an entire room. The room will be specified by input file. But it's not so easy. There are obstacles in the room and the robot has to avoid them. The robot has limited instruction set. The map is represented by an array, but index of each item finaly reprezent X,Y. The size and the input of the map will be provided in the file. There will be a recharge terminal in the map, where the robot can recharge his energy. But it has to disable the cleaner before the recharging is enabled.


## Expected solution
It's expected the program will do just text console output only. It has to write commands to the console which are currently proccessed. It has also display the rest of battery. The details of the output is up to you.

*Requirements*
```
- Write current command which is the roboot currently processing
-- In case of detection write also result ('Obstacle', 'Clear')
- Write coordinates X,Y where the robot is currently placed
-- Will be also fine write the current state of cleanest for the current position
- Write direction where the robot is watching straight
- Write the rest of battery
```

## Details

### Instructions of the Robot
```
- RotateLeft(), cost 2 energy - rotate the robot by 90° to the left
- RotateRight(), cost 2 energy - rotate the robot by 90° to the right
- MoveForward(), cost 2 energy - move the robot forward by one squere
- MoveBackward(), cost 2 energy - move the robot backward by one squere
- DetectObstacleInFrontOfRobot(), cost 1 energy
- EnableCleaner(), cost 1 energy and it cost additionally 1 energy of all commands above when the cleaner is enabled - it's cleaning current square
- DisableCleaner(), cost 1 energy 
- EnableRecharging(), cost 1 energy, the robot is not possible recharging when the cleanning function is enabled
- DisableRecharging(), cost 1 energy
```

### The map (input data file)

The final map on which the implementation will be tested is hidden until the implementation is done. But there is example of the structure of the input file with the map data. It's JSON (more bellow).
There are parameters width and height which define the map size. There is also array and each field of it describe one place in the map. The array is one row of values. You can index them from zero to lenght-1 of the array.
The zero-index means coordinates [0,0] (top left corner),index 9 means [9,0] (top right corner), index 10 means [0,1] (second row, first place close to top left corner), index 90 means [0, 9] (bottom left corner), index 99 means [9,9] (bottom right corner).

*Content of the input file 'RobotCleanerInput.json'*
```
{
    "map": {
        "width": 10,
        "height": 10,
        "data": [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   3, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 2, 2, 2, 2, 0, 0, 0,
                   0, 0, 0, 2, 0, 0, 2, 0, 0, 0,
                   0, 0, 0, 2, 0, 0, 2, 0, 0, 0,
                   0, 0, 0, 2, 0, 0, 2, 0, 0, 0,
                   0, 0, 0, 2, 0, 2, 2, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                    ]
    },
	"robot": {
		"direction": "east",
		"battery-capacity": 100
	}
}
```

*The meaing of numbers of the map array*
```
0 - is dirty place, but there is not obstacle
2 - is the place where is obstacle and robot can NOT move at this place
3 - is the recharge terminal - it is also place where the robot is starting
```

### The robot, energy (input data file)

As you can see, there are also specified some data about the robot. Mainly the battery capacity. You have to bear in the mind the robot has to be able always get back to the recharge terminal even all position are not cleaned yet.
There is also entry direction of the robot at the startup. 

*Possible values of a direction of the robot*
```
- east
- west
- north
- south
```

> The Challenge winner is who will provide the best of implementation. To the rating will be included those factors

- how many fields of the entire room has robot successfuly cleared
- how much energy it cost
- the implementation success, code complexity, an ideas and etc
- the pass time (basically if all previose ratings will be same, the earlier response will win)


** JSON **

Json is text file (containing only human readable characters) which represent some data. It's very common kind of the file format and it's usually supported any programming language or technology.

** Array **

A data in a memory are stored in one long block. Each cell of the memory has address. But for purpose of the map we need some two dimension array. But it is pretty easy. Just imagine, the first 10 cells are first row, next 10 cells are in second row and etc. For example you have index = 0 as adress to the block of memory means [0,0] coordiantes and index 10 means [0,1] coordinates.

